const items=[
    {
        "id":1,
        "name":"Strawberry Cake",
        "cost":400
    },
    {
        "id":2,
        "name":"Angel Food Cake",
        "cost":800
    },
    {
        "id":3,
        "name":"Black Forest Cake",
        "cost":500
    },
    {
        "id":4,
        "name":"Black Forest Cake",
        "cost":800
    },
    {
        "id":5,
        "name":"Brownies",
        "cost":300
    },
    {
        "id":6,
        "name":"Carrot and Walnut Cake",
        "cost":750
    }
];

var TotalAmount = 0;
var Itemcount = 0;
function AddToCart(id)
{
    var itemid = id;
    var item=items.find(item => item.id === itemid);
    console.log(item);
	var itemcost = item.cost;
	var itemname = item.name;
	var quantity = 1;
	Itemcount += 1;
	TotalAmount += quantity * itemcost;
	
	var cart = document.getElementById("carttable");
	cart.innerHTML +=
	`
	<tr>
		<th>${Itemcount}</th>
		<th>${itemname}</th>
		<th>${quantity}</th>
		<th>${itemcost}</th>
		<th>${quantity * itemcost}</th>
	</tr>
	`
	
	var cartTotal = document.getElementById("cartTotal")
	cartTotal.innerHTML +=
	`
	<tr>
		<th></th>
		<th></th>
		<th></th>
		<th><b>TOTAL</b></th>
		<th>${TotalAmount}</th>
	</tr>
	`
}